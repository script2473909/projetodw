import 'bootstrap';
import HostTable from './components/HostTable';
import HostForm from './components/HostForm';
import API from '../js/components/api.js';

const host = await API.read("hosts");

import 'bootstrap/dist/css/bootstrap.css';

for (const hostss of host) {
    HostTable.insert(hostss);
}

window.handleSubmit = HostForm.handleSubmit;