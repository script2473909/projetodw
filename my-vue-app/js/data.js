export const hosts = [
    {
      name: 'Google DNS',
      address: '8.8.8.8',
      version: 'v4',
    },
    {
      name: 'Google Search',
      address: 'www.google.com',
      version: 'v4',
    },
    {
      name: 'Cloudflare DNS',
      address: '1.1.1.1',
      version: 'v4',
    },
  ];
